@echo off
TITLE Aion 2.7 - Game Server Console
color 4
REM Start...
:start
echo Starting Aion 2.7 Game Server
echo.

SET PATH="C:\Program Files\Java\JavaJDK_6\bin"

REM -------------------------------------
REM Default parameters for a basic server.
java -Xms512m -Xmx1536m -Xdebug -ea -javaagent:./libs/al-commons-1.3.jar -cp ./libs/*;AL-Game.jar com.aionemu.gameserver.GameServer
REM -------------------------------------

SET CLASSPATH=%OLDCLASSPATH%

if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
if ERRORLEVEL 0 goto end

REM Restart...
:restart
echo.
echo Administrator Restart ...
echo.
goto start

REM Error...
:error
echo.
echo Server terminated abnormaly ...
echo.
goto end

REM End...
:end
echo.
echo Server terminated ...
echo.
pause
