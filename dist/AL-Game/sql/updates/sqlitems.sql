-- Armas asmos e elyos 

INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES
(1, 100900571, 1, 100, 4, 0, 10, 'Lannoks Greatsword'),
(2, 101300543, 1, 100, 4, 0, 1, 'Lannoks Spear'),
(3, 101500571, 1, 100, 4, 0, 1, 'Lannok Staff'),
(4, 101700620, 1, 80, 4, 0, 1, 'Tahabata Longbow'),
(5, 101700620, 1, 80, 3, 0, 1, 'Tahabata Longbow'),
(6, 100000768, 1, 80, 4, 0, 1, 'Tahabata Sword'),
(7, 100000768, 1, 80, 3, 0, 1, 'Tahabata Sword'),
(8, 100200702, 1, 80, 4, 0, 1, 'Tahabata Dagger'),
(9, 100200702, 1, 80, 3, 0, 1, 'Tahabata Dagger'),
(10, 100100581, 1, 80, 4, 1, 1, 'Tahabata Warhammer'),
(11, 100100581, 1, 80, 3, 1, 1, 'Tahabata Warhammer'),
(12, 100500602, 1, 80, 4, -1, 0, 'Tahabata Jewel'),
(13, 100500602, 1, 80, 3, -1, 0, 'Tahabata Jewel'),
(14, 100600636, 1, 80, 4, 0, 1, 'Tahabata Tome'),
(15, 100600636, 1, 80, 3, 0, 1, 'Tahabata Tome');


INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES
(16, 101300542, 1, 100, 3, 0, 1, 'Triroans Spear'),
(17, 101500570, 1, 100, 3, 0, 1, 'Triroans Staff'),
(18, 100900570, 1, 100, 3, 0, 1, 'Triroans Greatsword');


--Set arena pvp c1 asmos e elyos


INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES
(100, 110601256, 1, 30, 5, 0, 10, 'Divine Breastplate'),
(101, 110601256, 1, 30, 6, 0, 10, 'Divine Breastplate'),
(102, 110601270, 1, 30, 5, 0, 1, 'Sublime Breastplate'),
(103, 110601270, 1, 30, 6, 0, 1, 'Sublime Breastplate'),

(104, 110501283, 1, 30, 5, 0, 1, 'Divine Hauberk'),
(105, 110501283, 1, 30, 6, 0, 1, 'Divine Hauberk'),
(106, 110501296, 1, 30, 5, 0, 1, 'Sublime Hauberk'),
(107, 110501296, 1, 30, 6, 0, 1, 'Sublime Hauberk'),


(108, 110301304, 1, 30, 5, 0, 1, 'Divine Jerkin'),
(109, 110301304, 1, 30, 6, 0, 1, 'Divine Jerkin'),
(110, 110301319, 1, 30, 5, 0, 1, 'Sublime Jerkin'),
(111, 110301319, 1, 30, 6, 0, 1, 'Sublime Jerkin'),


(112, 110101378, 1, 30, 5, 0, 1, 'Divine Tunic'),
(113, 110101378, 1, 30, 6, 1, 1, 'Divine Tunic'),
(114, 110101392, 1, 30, 5, 1, 1, 'Sublime Tunic'),
(115, 110101392, 1, 30, 6, -1, 0, 'Sublime Tunic');


--calça
INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES
(150, 113601212, 1, 22, 5, 0, 10, 'Divine Greaves'),
(151, 113601212, 1, 22, 6, 0, 10, 'Divine Greaves'),
(152, 113601226, 1, 22, 5, 0, 1, 'Sublime Greaves'),
(153, 113601226, 1, 22, 6, 0, 1, 'Sublime Greaves'),

(154, 113501259, 1, 22, 5, 0, 1, 'Divine Chausses'),
(155, 113501259, 1, 22, 6, 0, 1, 'Divine Chausses'),
(156, 113501272, 1, 22, 5, 0, 1, 'Sublime Chausses'),
(157, 113501272, 1, 22, 6, 0, 1, 'Sublime Chausses'),


(158, 113301273, 1, 22, 5, 0, 1, 'Divine Breeches'),
(159, 113301273, 1, 22, 6, 0, 1, 'Divine Breeches'),
(160, 113101286, 1, 22, 5, 0, 1, 'Sublime Breeches'),
(161, 113101286, 1, 22, 6, 0, 1, 'Sublime Breeches'),


(162, 113101271, 1, 22, 5, 0, 1, 'Divine Leggings'),
(163, 113101271, 1, 22, 6, 1, 1, 'Divine Leggings'),
(164, 113101286, 1, 22, 5, 1, 1, 'Sublime Leggings'),
(165, 113101286, 1, 22, 6, -1, 0, 'Sublime Leggings');

--botas

INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES

(200, 114601205, 1, 15, 5, 0, 10, 'Divine Sabatons'),
(201, 114601205, 1, 15, 6, 0, 10, 'Divine Sabatons'),
(202, 114601222, 1, 15, 5, 0, 1, 'Sublime Sabatons'),
(203, 114601222, 1, 15, 6, 0, 1, 'Sublime Sabatons'),

(204, 114501262, 1, 15, 5, 0, 1, 'Divine Brogans'),
(205, 114501262, 1, 15, 6, 0, 1, 'Divine Brogans'),
(206, 114501279, 1, 15, 5, 0, 1, 'Sublime Brogans'),
(207, 114501279, 1, 15, 6, 0, 1, 'Sublime Brogans'),


(208, 114301304, 1, 15, 5, 0, 1, 'Divine Boots'),
(209, 114301304, 1, 15, 6, 0, 1, 'Divine Boots'),
(210, 114301321, 1, 15, 5, 0, 1, 'Sublime Boots'),
(211, 114301321, 1, 15, 6, 0, 1, 'Sublime Boots'),


(212, 114101298, 1, 15, 5, 0, 1, 'Divine Shoes'),
(213, 114101298, 1, 15, 6, 1, 1, 'Divine Shoes'),
(214, 114301321, 1, 15, 5, 1, 1, 'Sublime Shoes'),
(215, 114301321, 1, 15, 6, -1, 0, 'Sublime Shoes');



--luvas

INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES

(250, 111601221, 1, 15, 5, 0, 10, 'Divine Gauntlets'),
(251, 111601221, 1, 15, 6, 0, 10, 'Divine Gauntlets'),
(252, 111601238, 1, 15, 5, 0, 1, 'Sublime Gauntlets'),
(253, 111601238, 1, 15, 6, 0, 1, 'Sublime Gauntlets'),

(254, 111501241, 1, 15, 5, 0, 1, 'Divine Handguards'),
(255, 111501241, 1, 15, 6, 0, 1, 'Divine Handguards'),
(256, 111501258, 1, 15, 5, 0, 1, 'Sublime Handguards'),
(257, 111501258, 1, 15, 6, 0, 1, 'Sublime Handguards'),


(258, 111301248, 1, 15, 5, 0, 1, 'Divine Vambrace'),
(259, 111301248, 1, 15, 6, 0, 1, 'Divine Vambrace'),
(260, 111301263, 1, 15, 5, 0, 1, 'Sublime Vambrace'),
(261, 111301263, 1, 15, 6, 0, 1, 'Sublime Vambrace'),


(262, 111101253, 1, 15, 5, 0, 1, 'Divine Gloves'),
(263, 111101253, 1, 15, 6, 1, 1, 'Divine Gloves'),
(264, 111101269, 1, 15, 5, 1, 1, 'Sublime Gloves'),
(265, 111101269, 1, 15, 6, -1, 0, 'Sublime Gloves');


--ombreira

INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES

(300, 110601256, 1, 15, 5, 0, 10, 'Divine Shoulder'),
(301, 110601256, 1, 15, 6, 0, 10, 'Divine Shoulder'),
(302, 112601213, 1, 15, 5, 0, 1, 'Sublime Epaulettes'),
(303, 112601213, 1, 15, 6, 0, 1, 'Sublime Epaulettes'),

(304, 112301193, 1, 15, 5, 0, 1, 'Divine Spaulders'),
(305, 112301193, 1, 15, 6, 0, 1, 'Divine Spaulders'),
(306, 112501200, 1, 15, 5, 0, 1, 'Sublime Spaulders'),
(307, 112501200, 1, 15, 6, 0, 1, 'Sublime Spaulders'),


(308, 112301193, 1, 15, 5, 0, 1, 'Divine Shoulder'),
(309, 112301193, 1, 15, 6, 0, 1, 'Divine Shoulder'),
(310, 112301207, 1, 15, 5, 0, 1, 'Sublime Shoulder'),
(311, 112301207, 1, 15, 6, 0, 1, 'Sublime Shoulder'),


(312, 112101212, 1, 15, 5, 0, 1, 'Divine Pauldrons'),
(313, 112101212, 1, 15, 6, 1, 1, 'Divine Pauldrons'),
(314, 112101226, 1, 15, 5, 1, 1, 'Sublime Epaulettes'),
(315, 112101226, 1, 15, 6, -1, 0, 'Sublime Epaulettes');


--helm

INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES

(350, 125002971, 1, 30, 5, 0, 10, 'Divine Helm'),
(351, 125002971, 1, 30, 6, 0, 10, 'Divine Helm'),
(352, 125003002, 1, 30, 5, 0, 1, 'Sublime Helm'),
(353, 125003002, 1, 30, 6, 0, 1, 'Sublime Helm'),

(354, 125002963, 1, 30, 5, 0, 1, 'Divine Hood'),
(355, 125002963, 1, 30, 6, 0, 1, 'Divine Hood'),
(356, 125003000, 1, 30, 5, 0, 1, 'Sublime Hood'),
(357, 125003000, 1, 30, 6, 0, 1, 'Sublime Hood'),


(358, 125002956, 1, 30, 5, 0, 1, 'Divine Hat'),
(359, 125002956, 1, 30, 6, 0, 1, 'Divine Hat'),
(360, 125002998, 1, 30, 5, 0, 1, 'Sublime Hat'),
(361, 125002998, 1, 30, 6, 0, 1, 'Sublime Hat'),


(362, 125002951, 1, 30, 5, 0, 1, 'Divine Bandana'),
(363, 125002951, 1, 30, 6, 1, 1, 'Divine Bandana'),
(364, 125002996, 1, 30, 5, 1, 1, 'Sublime Bandana'),
(365, 125002996, 1, 30, 6, -1, 0, 'Sublime Bandana');



--manastones

INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES

(400, 167000558, 10, 20, 7, 0, 10, 'Crit Strike+17'),
(401, 167000518, 10, 20, 7, 0, 10, 'Attack +5'),
(402, 167000553, 10, 20, 7, 0, 10, 'Accuracy +27'),
(403, 167000560, 10, 20, 7, 0, 10, 'Magical Accuracy +14'),
(404, 167000555, 10, 20, 7, 0, 10, 'Magic Boost +27'),
(405, 167000557, 10, 20, 7, 0, 10, 'Block +27'),
(406, 167000561, 10, 20, 7, 0, 10, 'Resist Magic +14')
(407, 166000190, 1, 20, 7, 0, 10, 'L190 Enchantment');

--godstone

INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES

(450, 168000066, 1, 40, 7, 0, 10, 'Godstone: Traufnir'),
(451, 168000067, 1, 40, 7, 0, 10, 'Godstone: Suthran'),
(452, 168000068, 1, 40, 7, 0, 10, 'Godstone: Sifs Root'),
(453, 168000069, 1, 40, 7, 0, 10, 'Godstone: Sigyn'),
(454, 168000070, 1, 40, 7, 0, 10, 'Godstone: Aegirs'),
(455, 168000071, 1, 40, 7, 0, 10, 'Godstone: Neritas'),
(456, 168000072, 1, 40, 7, 0, 10, 'Godstone: Freyrs');


--pets

INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES
(500, 900118, 1, 5, 9, 0, 10, 'Red Diabol'),
(501, 190020138, 1, 5, 9, 0, 10, 'Stormwing Egg');

--scrolls


INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES
(550, 164000076, 1000, 15, 10, 0, 10, 'Greater Running'),
(551, 164000134 , 1000, 15, 10, 0, 10, 'Greater Awakening '),
(552, 164000073 , 1000, 15, 10, 0, 10, 'Greater Courage');

--food


INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES
(600, 160010097, 200, 15, 11, 0, 10, 'Polar Bear Candy'),
(601, 160010121, 200, 15, 11, 0, 10, 'Inquin Form'),
(602, 160010122, 200, 15, 11, 0, 10, 'Inquin Form');



--amuletos
INSERT INTO `ingameshop` (`object_id`, `item_id`, `item_count`, `item_price`, `category`, `list`, `sales_ranking`, `description`) VALUES
(600, 169620010, 5, 12, 14, 0, 10, 'Polar Bear Candy');

